call plug#begin('~/.config/nvim/plugins')
" Local config with confirmation
Plug 'MunifTanjim/exrc.nvim'

Plug 'rcarriga/nvim-notify'

" Chezmoi
Plug 'alker0/chezmoi.vim'
Plug 'Lilja/vim-chezmoi'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'ryanoasis/vim-devicons'

" General text editing
Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'kristijanhusak/defx-git'
Plug 'kristijanhusak/defx-icons'
Plug 'scrooloose/nerdcommenter'
Plug 'simnalamburt/vim-mundo'
Plug 'ggandor/leap.nvim'
Plug 'tommcdo/vim-exchange'
Plug 'editorconfig/editorconfig-vim'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'tpope/vim-surround'
Plug 'qpkorr/vim-bufkill'
" Restore FocusGained, FocusLost events in tmux
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'chrisbra/unicode.vim'
Plug 'ruanyl/vim-gh-line'
Plug 'tpope/vim-projectionist'
" Guess indentation
Plug 'NMAC427/guess-indent.nvim'

" Text snippets
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'

" formatter.nvim
Plug 'mhartington/formatter.nvim'

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'

" LSP client
Plug 'neovim/nvim-lspconfig'

" Autocompletion
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

" Distraction-free writing
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'

" Git
Plug 'tpope/vim-fugitive'

" wisely add "end" in ruby, Lua, Vimscript, etc.
Plug 'RRethy/nvim-treesitter-endwise'
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Automatic folding for RSpec files
Plug 'rlue/vim-fold-rspec'

" fuzzy finder
Plug 'nvim-telescope/telescope.nvim'
  Plug 'nvim-lua/popup.nvim'
  Plug 'nvim-lua/plenary.nvim'

" Handle file:<linenumber>:<columnnumber>
Plug 'wsdjeg/vim-fetch'

"A duck
Plug 'tamton-aquib/duck.nvim'

"slim-template
Plug 'slim-template/vim-slim'
call plug#end()

""""" Vim settings
set termguicolors
set background=dark
colorscheme solarized

" General settings
set showcmd
set showmatch
set showmode
set ruler
set number
set relativenumber
set cursorline
set undofile
set undodir=~/.config/nvim/undo
set undolevels=500
set undoreload=10000
set nowrap
set noshowmode
set autowrite
set nobackup
set autoread
set noconfirm
set laststatus=2
set mouse=
set expandtab
set hidden
set tags=./.tags

" Automatically refresh file contents
set autoread
au CursorHold,CursorHoldI * checktime
au FocusGained,BufEnter * :checktime

" File ignoring
set wildignore=*.a,*.o,*.so,*.pyc,*.jpg,
        \*.jpeg,*.png,*.gif,*.pdf,*.git,
        \*.swp,*.swo
set wildmenu
set wildmode=list:longest,full

" More natural splits
set splitbelow
set splitright

" Show whitespaces
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif
set list

" Use X11 clipboard since I'm goddamn lazy
set clipboard+=unnamedplus

augroup Makefile
  autocmd FileType make setlocal noexpandtab
augroup END

" Return to last edit position when opening files
augroup LastPosition
  autocmd! BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \       exe "normal! g`\"" |
        \ endif
augroup END

" Make sure we don't exceed 72 characters in git commit
augroup GitCommitLength
  autocmd Filetype gitcommit setlocal colorcolumn=72
augroup END

" Disable relative line numbering when entering Insert mode
augroup NoRelativeNumberOnInsert
  autocmd!
  autocmd InsertEnter * set norelativenumber
  autocmd InsertLeave * set relativenumber
augroup END

" If the file has more than 1000 lines
" show all matches in a split
if line('$') > 1000
  set inccommand=split
else
  set inccommand=nosplit
endif

" Folding
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set nofoldenable

""""" Keyboard maps
nnoremap <SPACE> <Nop>
let mapleader="\<Space>"

" Buffers are the new tabs

nnoremap gn :bnext<CR>
nnoremap gN :bprev<CR>

" Treat wraped lines as normal lines
nnoremap j gj
nnoremap k gk

" Ex more pls go
map Q <nop>

nmap <silent> <leader>s :set spell!<CR>

""""" Plugin specific settings

" MunifTanjim/exrc.nvim
lua << EOF
  vim.o.exrc = false
  require("exrc").setup {
    files = {
      ".nvimrc.lua",
      ".nvimrc",
      ".exrc.lua",
      ".exrc",
    },
  }
EOF

" nvim-treesitter
lua << EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    "ruby",
    "javascript",
    "python",
    "vim",
    "vimdoc",
    "bash",
    "vue",
    "toml",
    "terraform",
    "svelte",
    "sql",
    "rust",
    "regex",
    "graphql",
    "git_config",
    "git_rebase",
    "gitattributes",
    "gitcommit",
    "gitignore",
    "dockerfile",
    "css"
  },
  highlight = {
    enable = true
  },
  indent = {
    enable = true
  },
  endwise = {
    enable = true
  }
}
EOF

" ms-jpq/coq_nvim
let g:coq_settings = { 'auto_start': 'shut-up', 'xdg': v:true }

" nvim-lspconfig
lua << EOF
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', vim.lsp.buf.format, bufopts)
end

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

local coq = require "coq"
require('lspconfig').hls.setup(coq.lsp_ensure_capabilities{
    on_attach = on_attach,
    flags = lsp_flags,
    }
)
require("lspconfig").ruby_ls.setup(coq.lsp_ensure_capabilities{
    on_attach = on_attach,
    flags = lsp_flags,
    }
)
require('lspconfig')['solargraph'].setup(coq.lsp_ensure_capabilities{
    on_attach = on_attach,
    flags = lsp_flags,
    }
)
vim.api.nvim_create_autocmd("BufWritePre", {
        pattern = "ruby",
        callback = function()
          vim.lsp.buf.format()
        end,
      }
)
EOF

" Lilja/vim-chezmoi
let g:chezmoi = "enabled"

" Goyo
let g:goyo_width = "60%"
let g:goyo_height = "85%"
autocmd User GoyoEnter Limelight
autocmd User GoyoLeave Limelight!
nnoremap <Leader>l :Goyo<CR>

" Gundo
let g:mundo_right = 1
map <F3> :MundoToggle<CR>

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
let g:bufferline_echo = 0

" Create parent directories if they don't exist
" http://vi.stackexchange.com/questions/678/how-do-i-save-a-file-in-a-directory-that-does-not-yet-exist
augroup Mkdir
  autocmd!
  autocmd BufWritePre *
    \ if !isdirectory(expand("<afile>:p:h")) |
        \ call mkdir(expand("<afile>:p:h"), "p") |
    \ endif
augroup END

"bufkill.vim
map <C-d> :BD<CR>

" formatter.nvim
lua << EOF
  local util = require "formatter.util"
  require("formatter").setup {
    logging = true,
    log_level = vim.log.levels.DEBUG,
    filetype = {
      ["*"] = {
        require("formatter.filetypes.any").remove_trailing_whitespace
      }
    }
  }
EOF

augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost * FormatWriteLock
augroup END

nnoremap <silent> <leader>f :FormatLock<CR>
nnoremap <silent> <leader>F :FormatWriteLock<CR>

" defx
nnoremap <silent> <Leader>t :Defx<CR>
call defx#custom#option('_', {
                        \ 'winwidth': 40,
                        \ 'split': 'vertical',
                        \ 'direction': 'topleft',
                        \ 'show_ignored_files': 1,
                        \ 'toggle': 1,
                        \ 'resume': 1,
                        \ 'columns': 'git:icons:indent:filename:type',
                        \ })

call defx#custom#column('icon', {
                        \ 'directory_icon': '▸',
                        \ 'opened_icon': '▾',
                        \ })

autocmd FileType defx call s:defx_my_settings()
function! s:defx_my_settings() abort
  " Define mappings
  nnoremap <silent><buffer><expr> <CR>
  \ defx#do_action('drop')
  nnoremap <silent><buffer><expr> c
  \ defx#do_action('copy')
  nnoremap <silent><buffer><expr> m
  \ defx#do_action('move')
  nnoremap <silent><buffer><expr> p
  \ defx#do_action('paste')
  nnoremap <silent><buffer><expr> l
  \ defx#do_action('open')
  nnoremap <silent><buffer><expr> E
  \ defx#do_action('open', 'vsplit')
  nnoremap <silent><buffer><expr> P
  \ defx#do_action('preview')
  nnoremap <silent><buffer><expr> o
  \ defx#do_action('open_tree', 'toggle')
  nnoremap <silent><buffer><expr> K
  \ defx#do_action('new_directory')
  nnoremap <silent><buffer><expr> N
  \ defx#do_action('new_file')
  nnoremap <silent><buffer><expr> M
  \ defx#do_action('new_multiple_files')
  nnoremap <silent><buffer><expr> C
  \ defx#do_action('toggle_columns',
  \                'mark:indent:icon:filename:type:size:time')
  nnoremap <silent><buffer><expr> S
  \ defx#do_action('toggle_sort', 'time')
  nnoremap <silent><buffer><expr> d
  \ defx#do_action('remove')
  nnoremap <silent><buffer><expr> r
  \ defx#do_action('rename')
  nnoremap <silent><buffer><expr> !
  \ defx#do_action('execute_command')
  nnoremap <silent><buffer><expr> x
  \ defx#do_action('execute_system')
  nnoremap <silent><buffer><expr> yy
  \ defx#do_action('yank_path')
  nnoremap <silent><buffer><expr> .
  \ defx#do_action('toggle_ignored_files')
  nnoremap <silent><buffer><expr> ;
  \ defx#do_action('repeat')
  nnoremap <silent><buffer><expr> h
  \ defx#do_action('cd', ['..'])
  nnoremap <silent><buffer><expr> ~
  \ defx#do_action('cd')
  nnoremap <silent><buffer><expr> q
  \ defx#do_action('quit')
  nnoremap <silent><buffer><expr> <Space>
  \ defx#do_action('toggle_select') . 'j'
  nnoremap <silent><buffer><expr> *
  \ defx#do_action('toggle_select_all')
  nnoremap <silent><buffer><expr> j
  \ line('.') == line('$') ? 'gg' : 'j'
  nnoremap <silent><buffer><expr> k
  \ line('.') == 1 ? 'G' : 'k'
  nnoremap <silent><buffer><expr> <C-l>
  \ defx#do_action('redraw')
  nnoremap <silent><buffer><expr> <C-g>
  \ defx#do_action('print')
  nnoremap <silent><buffer><expr> cd
  \ defx#do_action('change_vim_cwd')
endfunction

"vim-gh-line
let g:gh_line_map = '<leader>gh'
let g:gh_line_blame_map = '<leader>gb'
let g:gh_line_repo_map = '<leader>go'
let g:gh_open_command = 'fn() { echo "$@" | wl-copy; }; fn '

"ggandor/leap.nvim
lua << EOF
require('leap').set_default_keymaps()
EOF

"telescope.nvim
nnoremap <leader>p <cmd>Telescope git_files<cr>
nnoremap <leader>o <cmd>Telescope find_files<cr>
nnoremap <leader>g <cmd>Telescope live_grep<cr>
nnoremap <leader>; <cmd>Telescope buffers<cr>

" Copy current file position as rspec command
nnoremap <silent><leader>ss :let @+="bundle exec spring rspec " . expand("%") . ":" . line(".")<CR>

"NMAC427/guess-indent.nvim
lua << EOF
require('guess-indent').setup {}
EOF

lua << EOF
vim.keymap.set('n', '<leader>dd', function() require("duck").hatch() end, {})
vim.keymap.set('n', '<leader>dk', function() require("duck").cook() end, {})
EOF

set backupcopy=yes
